package ddanilov.step2018.myquize.data

import android.content.Context
import ddanilov.step2018.myquize.model.User

object SharedPreferencesHelper {
    private const val propertyNameName = "Name"
    private const val propertyEmailEmail = "Email"
    val propetiesName = "Property"
    fun getUser(context: Context): User? {
        context.getSharedPreferences(propetiesName, Context.MODE_PRIVATE).apply {
            val name = getString("Name", null)
            val email = getString("Email", null)
            name?.run {
                email?.run {
                    return User(name, email)
                }
            }
        }
        return null
    }

    fun setUser(context: Context,name: String,email:String){
        context.getSharedPreferences(propetiesName,Context.MODE_PRIVATE).edit().apply{
            putString(propertyNameName,name)
            putString(propertyEmailEmail,email)
        }
    }
}